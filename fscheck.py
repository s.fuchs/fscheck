#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#author: Stephan Fuchs (Robert Koch Institute, MF1, fuchss@rki.de)

import os
import re
import sys
import argparse
from joblib import Parallel, delayed
from Bio import Align
from Bio import SeqIO

def parse_args():
	parser = argparse.ArgumentParser(prog="fscheck.py", description="checks sequences for frameshift mutations")
	parser.add_argument('-f', metavar="FILE", help="fasta file containing the reference sequence (default: NC_045512.2 is used as reference)", type=str, default=None)
	parser.add_argument('-g', metavar="FILE", help="gff file containing the reference annotation (default: NC_045512.2 is used as reference)", type=str, default=None)
	parser.add_argument('-t', metavar="INT", help="cpus to use (default: 1)", type=int, default=1)
	parser.add_argument('-s', metavar="FILE", help="sequence report listing frameshift information for each sequence (default: fscheck_sequence-report.tsv). Warning: Existing files will be overwrittens", type=str, default="fscheck_list.tsv")
	parser.add_argument('-i', metavar="FILE", help="frameshift report and their frequencies across all sequences (default: fscheck_frameshift-report.tsv). Warning: Existing files will be overwrittens", type=str, default="fscheck_summary.tsv")
	parser.add_argument('fasta', metavar="FILE", help="fasta file(s) containing the sequence(s) to check", type=str, nargs="+")
	return parser.parse_args()

def worker(seqid, seq, refseq, cds):
	aligner = Align.PairwiseAligner()
	aligner.mode = 'global'
	aligner.open_gap_score = -0.5
	aligner.extend_gap_score = -0.1
	aligner.target_end_gap_score = 0.0
	aligner.query_end_gap_score = 0.0
	alignment = str(sorted(aligner.align(refseq, seq), key=lambda x: x.score, reverse=True)[0]).split("\n")

	target = []
	query = []
	s = 0
	for refnuc in re.finditer(".-*", alignment[0]):
		e = s + len(refnuc.group())
		query.append(alignment[2][s:e])
		s = e

	fs = []
	for gene in cds:
		gap = []
		for i, nuc in enumerate(query[gene[1]:gene[2]]):
			if nuc == "-" or nuc == "":
				gap.append("-")
				continue
			l = len(gap)
			if l%3 > 0:
				if l == 1:
					mutation = "DEL" + str(gene[1] + i - l + 1)
				else:
					mutation = "DEL" + str(gene[1] + i - l + 1) + "-" + str(gene[1] + i)
				fs.append((seqid, gene[0], mutation))
				gap = []
			elif l > 0:
				gap = []
			elif (len(nuc)-1)%3 > 0:
				mutation = nuc[0] + str(gene[1] + i - len(nuc) + 1) + nuc
				fs.append((seqid, gene[0], mutation))         
	return fs

def get_cds_from_gff(fname):
	gene_regex = re.compile("gene=([^;]+)(?:;|$)")
	cds = set()
	with open(fname, "r") as handle:
		for line in handle:
			if line.startswith("#") or len(line.strip()) == 0:
				continue
			fields = line.strip().split("\t")
			if fields[2] == "CDS":
				gene = gene_regex.search(fields[-1]).groups(1)
				strand = fields[6]
				s, e  = min(int(fields[3]),int(fields[4])), max(int(fields[3]),int(fields[4]))
				cds.add((gene[0], s, e, strand))
	return cds

def get_seqs_from_fasta(fname):
	seqs = set()
	for record in SeqIO.parse(fname, "fasta"):
		seqs.add((str(record.id), str(record.seq)))
	return seqs

def get_seq_from_fasta(fname):
	return str(SeqIO.read(fname, "fasta").seq)

if __name__ == "__main__":
	args = parse_args()
	if not args.f:
		args.f = os.path.join(os.path.dirname(os.path.abspath(__file__)), "ref.fna")
	if not args.g:
		args.g = os.path.join(os.path.dirname(os.path.abspath(__file__)), "ref.gff3")

	seqs = set()
	for f in args.fasta:
		seqs.update(get_seqs_from_fasta(f))

	print("processing", len(seqs), "sequence(s) using", args.t, "thread(s) ...")

	ids = [x[0] for x in seqs]
	if len(ids) != len(set(ids)):
		sys.exit("error: accessions not unique")

	refseq = get_seq_from_fasta(args.f)
	cds = get_cds_from_gff(args.g)
	fss = Parallel(n_jobs=args.t)(delayed(worker)(x, y, refseq, cds) for x, y in seqs)

	fss = [item for sublist in fss for item in sublist]
	mutations = sorted([x[2] for x in fss])

	with open(args.s, "w") as handle:
		handle.write("# sequence\tcds\tframe_shifting_mutation\n")
		for fs in sorted(fss):
			handle.write("\t".join(fs) + "\n")
    
	total = len(seqs)
	with open(args.i, "w") as handle:
		handle.write("# frame_shifting_mutation\toccurences\t%occurences\n")
		for m in mutations:
			n = mutations.count(m)
			handle.write("\t".join([m, str(n),  str(round(n/total, 2))]) + "\n")

	print(len(set(mutations)), "frame shifting mutations found in", len(set([x[0] for x in fss])), "sequences.")
