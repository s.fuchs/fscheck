#!/usr/bin/env bash

# set bash mode to strict
set -euo pipefail
IFS=$'\n\t'

# Test directory
TEST_DIR=${TMPDIR:-.}/test-${USER:-}
echo 'writing to '"${TEST_DIR:-.}"
mkdir -p "${TEST_DIR:-.}"

echo 'setting up testing environment...'
conda env create --force --file fscheck.env.yml


echo "executing tests"
source $(conda info --root)/etc/profile.d/conda.sh
set +u
conda activate fscheck_clitest
set -u
./fscheck.py -s ${TEST_DIR}/sequence-report.tsv -i ${TEST_DIR}/frameshift-report.tsv test/test.fasta
conda deactivate

if cmp -s "${TEST_DIR}/sequence-report.tsv" "test/expected_sequence-report.tsv"; then
    printf 'sequence report as expected'
else
    printf 'sequence report not as expected'
	diff "${TEST_DIR}/sequence-report.tsv" "test/expected_sequence-report.tsv"
	conda env remove -y --name fscheck_clitest
	exit 1
fi

if cmp -s "${TEST_DIR}/frameshift-report.tsv" "test/expected_frameshift-report.tsv"; then
    echo 'frameshift report as expected'
else
    echo 'frameshift report not as expected'
	diff "${TEST_DIR}/frameshift-report.tsv" "test/expected_frameshift-report.tsv"
	conda env remove --name fscheck_clitest
	exit 1
fi

conda env remove -y --name fscheck_clitest

