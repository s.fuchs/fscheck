# fscheck

A tool for detecting frameshift mutations in genome sequences. 

## install

```sh
# enter the designated folder
cd folder
# clone the git repo
git clone https://gitlab.com/s.fuchs/fscheck
# test
cd fscheck; ./test.sh
```

## dependencies

|tool       |version  |
|-----------|---------|
| python    | >=3.8.8 |
| biopython | >=1.78  |
| joblib    | >=1.0.1 |


You can create a conda environment to meet all requirements.

```sh
# create a conda environment
conda env create --file path/to/fscheck/fscheck.env.yml
```

## usage
```sh
# activate conda environment if created (see dependencies)
conda activate fscheck
# screen all sequences is seqs.fna for frame shift mutations 
# based on NC_045512.2 (SARS-CoV-2 Wuhan-1 reference sequence)
# using 10 cpus
path/to/fscheck.py -t 10 seqs.fna
```

## options
```sh
# show all available options
path/to/fscheck.py -h
```

## output
By default two tab-delimited text files are generated in the current working directory:
- fscheck_sequence-report.tsv
- fscheck_frameshift-report.tsv

The sequence report (fscheck_sequence-report.tsv) contains the following fields:
- sequence accession
- gene symbol
- frameshift mutation

The frameshift report (fscheck_frameshift-report.tsv) contains the following fields:
- frameshift mutation
- absolute number of sequences containing the respective mutation 
- relative number of sequences containing the respective mutation 
